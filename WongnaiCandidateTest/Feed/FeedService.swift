//
//  FeedService.swift
//  WongnaiCandidateTest
//
//  Created by Chavin Charoenvitvorakul on 18/4/2563 BE.
//  Copyright © 2563 Chavin Charoenvitvorakul. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol FeedServiceProtocol {
    func getFeed(currentPage: Int,
    _ success: @escaping (JSON) -> (),
    _ errorDescription: @escaping (_ error: String?) -> ())
}

class FeedService: FeedServiceProtocol {
    func getFeed(currentPage: Int,
                 _ success: @escaping (JSON) -> (),
                 _ errorDescription: @escaping (_ error: String?) -> ()) {
        AF.request("https://api.500px.com/v1/photos?feature=popular&page=\(currentPage)", method: .get).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                success(json)
            case let .failure(error):
                errorDescription(error.errorDescription)
            }
        }
    }
}
