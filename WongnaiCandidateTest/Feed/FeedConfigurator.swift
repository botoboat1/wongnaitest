//
//  FeedConfigurator.swift
//  WongnaiCandidateTest
//
//  Created by Chavin Charoenvitvorakul on 18/4/2563 BE.
//  Copyright © 2563 Chavin Charoenvitvorakul. All rights reserved.
//

import UIKit

final class FeedConfigurator {

  static let shared = FeedConfigurator()

  func configure(viewController: FeedViewController) {
    
    let worker = FeedWorker()
    worker.service = FeedService()

    let presenter = FeedPresenter()
    presenter.controller = viewController

    let interactor = FeedInteractor()
    interactor.presenter = presenter
    interactor.worker = worker
    
    viewController.interactor = interactor
  }
}
