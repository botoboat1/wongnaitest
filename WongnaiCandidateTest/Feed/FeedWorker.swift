//
//  FeedWorker.swift
//  WongnaiCandidateTest
//
//  Created by Chavin Charoenvitvorakul on 18/4/2563 BE.
//  Copyright © 2563 Chavin Charoenvitvorakul. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol FeedWorkerProtocol {
    func getFeed(currentPage: Int,
                 _ success: @escaping ([FeedResponseModel]) -> (),
                 _ errorDescription: @escaping (_ error: String?) -> ())
}

class FeedWorker: FeedWorkerProtocol {
    
    var service: FeedServiceProtocol!
    
    func getFeed(currentPage: Int, _ success: @escaping ([FeedResponseModel]) -> (), _ errorDescription: @escaping (String?) -> ()) {
        service.getFeed(currentPage: currentPage, { (json) in
            var responseModel = [FeedResponseModel]()
            for photo in json["photos"].arrayValue {
                responseModel.append(FeedResponseModel(json: photo))
            }
            success(responseModel)
        }) { (error) in
            errorDescription(error)
        }
    }
}
