//
//  FeedPresenter.swift
//  WongnaiCandidateTest
//
//  Created by Chavin Charoenvitvorakul on 18/4/2563 BE.
//  Copyright © 2563 Chavin Charoenvitvorakul. All rights reserved.
//

import Foundation

protocol FeedPresenterProtocol {
    func reloadFeed(_ response: [FeedResponseModel])
    func insertFeed(_ response: [FeedResponseModel])
    func displayError(_ error: String?)
}

class FeedPresenter: FeedPresenterProtocol {
    
    weak var controller: FeedView!
    
    private var viewModel: [FeedViewModel]?
    
    func reloadFeed(_ response: [FeedResponseModel]) {
        
        self.viewModel = [FeedViewModel]()

        for model in response {

            if let imageViewModel = attachImageBetweemItem(index: self.viewModel?.count ?? 0) {
                self.viewModel?.append(imageViewModel)
            }

            let viewModel = transformResponseToViewModel(model)

            self.viewModel?.append(viewModel)
        }
        
        self.controller.reloadFeed(viewModel: viewModel)
    }
    
    func insertFeed(_ response: [FeedResponseModel]) {
        var indexPathRowUpdated = [IndexPath]()
        
        for (index, model) in response.enumerated() {

            if let imageViewModel = attachImageBetweemItem(index: self.viewModel?.count ?? 0) {
                indexPathRowUpdated.append(IndexPath(row: self.viewModel?.count ?? 0 + index, section: 0))
                self.viewModel?.append(imageViewModel)
            }

            let viewModel = transformResponseToViewModel(model)
            indexPathRowUpdated.append(IndexPath(row: self.viewModel?.count ?? 0 + index, section: 0))

            self.viewModel?.append(viewModel)
        }
        
        self.controller.insertFeed(viewModel: self.viewModel, indexPath: indexPathRowUpdated)
    }
    
    private func attachImageBetweemItem(index: Int) -> FeedViewModel? {
        if (index + 1) % 5 == 0 {
            var viewModel = FeedViewModel()
            viewModel.type = .image
            return viewModel
        } else {
            return nil
        }
    }
    
    private func transformResponseToViewModel(_ model: FeedResponseModel) -> FeedViewModel {
        var viewModel = FeedViewModel()
        viewModel.description = model.description
        if let image = model.image,
            let url = URL(string: image) {
            viewModel.imageUrl = url
        }
        viewModel.title = model.name
        viewModel.type = .item

        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        if let votedCount = model.votedCount,
            let formatString = formatter.string(from: votedCount) {
            viewModel.votedCountFormat = formatString
        }
        return viewModel
    }
    
    func displayError(_ error: String?) {
        
    }
}
