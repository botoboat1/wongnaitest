//
//  FeedItemType1Cell.swift
//  WongnaiCandidateTest
//
//  Created by Chavin Charoenvitvorakul on 17/4/2563 BE.
//  Copyright © 2563 Chavin Charoenvitvorakul. All rights reserved.
//

import UIKit
import Kingfisher

class FeedItemType1Cell: UITableViewCell {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var loveCountLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func setupView(viewModel: FeedViewModel?) {
        self.titleLabel.text = viewModel?.title
        self.descriptionLabel.text = viewModel?.description
        self.loveCountLabel.text = viewModel?.votedCountFormat
        self.logoImageView.kf.setImage(with: viewModel?.imageUrl)
    }
}
