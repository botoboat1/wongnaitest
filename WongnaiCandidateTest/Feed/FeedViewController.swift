//
//  ViewController.swift
//  WongnaiCandidateTest
//
//  Created by Chavin Charoenvitvorakul on 17/4/2563 BE.
//  Copyright © 2563 Chavin Charoenvitvorakul. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol FeedView: NSObjectProtocol {
    func reloadFeed(viewModel: [FeedViewModel]?)
    func insertFeed(viewModel: [FeedViewModel]?, indexPath: [IndexPath])
}

class FeedViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var heightDictionary: [Int : CGFloat] = [:]
    
    var viewModel: [FeedViewModel]?
    
    var interactor: FeedInteractorProtocol!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        FeedConfigurator.shared.configure(viewController: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialView()
        self.interactor.reloadFeed()
    }
    
    private func initialView() {
        self.navigationItem.title = "Feed"
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "FeedItemType1Cell",
                                 bundle: Bundle(for: FeedViewController.self)),
                           forCellReuseIdentifier: "FeedItemType1Cell")
        tableView.register(UINib(nibName: "FeedItemType2Cell", bundle: Bundle(for: FeedViewController.self)), forCellReuseIdentifier: "FeedItemType2Cell")
        
        tableView.dataSource = self
        tableView.delegate = self
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.addTarget(self, action:#selector(handleRefreshControl),
                                                  for: .valueChanged)
    }
    
    @objc func handleRefreshControl() {
        self.interactor.reloadFeed()
        
        DispatchQueue.main.async {
            self.tableView.refreshControl?.endRefreshing()
        }
    }
}

extension FeedViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModel?[indexPath.row].type == FeedType.item,
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedItemType1Cell") as? FeedItemType1Cell {
            
            cell.selectionStyle = .none
            cell.setupView(viewModel: viewModel?[indexPath.row])
            return cell
            
        } else if let cell = tableView.dequeueReusableCell(withIdentifier: "FeedItemType2Cell") as? FeedItemType2Cell {
            
            cell.selectionStyle = .none
            return cell
            
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath.row] = cell.frame.size.height
        self.interactor.getMoreFeed(lastIndex: (viewModel?.count ?? 0) - 1,
                                    currentIndex: indexPath.row)
    }
}

extension FeedViewController: FeedView {
    func insertFeed(viewModel: [FeedViewModel]?, indexPath: [IndexPath]) {
        self.viewModel = viewModel
        self.tableView.insertRows(at: indexPath, with: .none)
    }
    
    func reloadFeed(viewModel: [FeedViewModel]?) {
        self.viewModel = viewModel
        self.tableView.reloadData()
    }
}

extension FeedViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath.row]
        return height ?? UITableView.automaticDimension
    }
}
