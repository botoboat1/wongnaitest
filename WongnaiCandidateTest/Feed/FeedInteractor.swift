//
//  FeedInteractor.swift
//  WongnaiCandidateTest
//
//  Created by Chavin Charoenvitvorakul on 18/4/2563 BE.
//  Copyright © 2563 Chavin Charoenvitvorakul. All rights reserved.
//

import Foundation

protocol FeedInteractorProtocol {
    var nextPage: Int { get set }
    func reloadFeed()
    func getMoreFeed(lastIndex: Int, currentIndex: Int)
}

class FeedInteractor: FeedInteractorProtocol {
    
    var presenter: FeedPresenterProtocol!
    var worker: FeedWorkerProtocol!
    
    var nextPage = 1
    
    private var isLoading: Bool = false
    
    func reloadFeed() {
        if isLoading == true { return }
        
        self.nextPage = 1
        getFeed { (response) in
            self.presenter.reloadFeed(response)
        }
    }
    
    func getMoreFeed(lastIndex: Int, currentIndex: Int) {
        if currentIndex == lastIndex {
            if isLoading == true { return }
            
            getFeed { (response) in
                self.presenter.insertFeed(response)
            }
        }
    }
    
    private func getFeed(success: @escaping ([FeedResponseModel]) -> ()) {
        isLoading = true
        
        worker.getFeed(currentPage: nextPage, { (response) in
            self.isLoading = false
            self.nextPage += 1
            success(response)
        }) { (error) in
            self.isLoading = false
            self.presenter.displayError(error)
        }
    }
}
