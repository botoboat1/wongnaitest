//
//  FeedResponseModel.swift
//  WongnaiCandidateTest
//
//  Created by Chavin Charoenvitvorakul on 18/4/2563 BE.
//  Copyright © 2563 Chavin Charoenvitvorakul. All rights reserved.
//

import Foundation
import SwiftyJSON

struct FeedResponseModel {
    
    var id: String?
    var votedCount: NSNumber?
    var image: String?
    var description: String?
    var name: String?
    
    init(json: JSON) {
        id = json["id"].string
        votedCount = json["positive_votes_count"].number
        image = json["image_url"].arrayValue.map { $0.stringValue }.first
        description = json["description"].string
        name = json["name"].string
    }
}
