//
//  FeedResponseModel.swift
//  WongnaiCandidateTest
//
//  Created by Chavin Charoenvitvorakul on 17/4/2563 BE.
//  Copyright © 2563 Chavin Charoenvitvorakul. All rights reserved.
//

import Foundation
import SwiftyJSON

struct FeedViewModel {
    
    var description: String?
    var title: String?
    var imageUrl: URL?
    var votedCountFormat: String?
    var type: FeedType?
}

enum FeedType {
    case item
    case image
}
